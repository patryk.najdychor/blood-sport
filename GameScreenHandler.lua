local screen

function pilcrow.changeGameScreen(screenName)
  if screen and screen.finalize then
    screen:finalize()
  end

  if not pilcrow.Screens[screenName] then
    error("No screen with a given name " .. screenName, 2)
  end

  screen = pilcrow.Screens[screenName]()

  if pilcrow.debug and pilcrow.debug.GameScreenHandler.screenChange then
    pilcrow.debug.print("GameScreenHandler", "Changed screen", {["Screen name"] = screenName})
  end

  love.graphics.setColor(1, 1, 1, 1)

  love.draw = function()
    screen:draw()
  end

  love.mousepressed = function(x, y, button, isTouch, presses)
    screen:mousePressed(x, y, button, isTouch, presses)
  end

  love.mousereleased = function(x, y, button, isTouch, presses)
    screen:mouseReleased(x, y, button, isTouch, presses)
  end

  love.update = function(dt)
    if pilcrow.debug then
      require("structures/lovebird").update()
    end
    screen:update(dt)
  end

  love.keypressed = function(key, scancode, isrepeat)
    screen:keyPressed(key, scancode, isrepeat)
  end

  love.keyreleased = function(key, scancode)
    screen:keyReleased(key, scancode)
  end
end
