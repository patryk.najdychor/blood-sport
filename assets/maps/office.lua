return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.2.4",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 128,
  height = 128,
  tilewidth = 32,
  tileheight = 32,
  nextlayerid = 24,
  nextobjectid = 16,
  properties = {},
  tilesets = {
    {
      name = "Building",
      firstgid = 1,
      filename = "../tilesets/city_building_walls_roofs.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/Building.png",
      imagewidth = 256,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {
        {
          name = "Skyscraper_Wall",
          tile = 33,
          properties = {}
        }
      },
      tilecount = 64,
      tiles = {
        {
          id = 0,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 1,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 2,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 3,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 4,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 5,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 8,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 11,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 12,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 13,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 16,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 17,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 18,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 19,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 20,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 21,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 22,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 24,
          properties = {
            ["impassable"] = true
          },
          terrain = { -1, -1, -1, 0 }
        },
        {
          id = 25,
          properties = {
            ["impassable"] = true
          },
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 26,
          properties = {
            ["impassable"] = true
          },
          terrain = { -1, -1, 0, -1 }
        },
        {
          id = 27,
          properties = {
            ["impassable"] = true
          },
          terrain = { 0, 0, 0, -1 }
        },
        {
          id = 28,
          properties = {
            ["impassable"] = true
          },
          terrain = { 0, 0, -1, 0 }
        },
        {
          id = 29,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 30,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 31,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 32,
          properties = {
            ["impassable"] = true
          },
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 33,
          properties = {
            ["impassable"] = true
          },
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 34,
          properties = {
            ["impassable"] = true
          },
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 35,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 36,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 37,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 38,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 39,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 40,
          properties = {
            ["impassable"] = true
          },
          terrain = { -1, 0, -1, -1 }
        },
        {
          id = 41,
          properties = {
            ["impassable"] = true
          },
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 42,
          properties = {
            ["impassable"] = true
          },
          terrain = { 0, -1, -1, -1 }
        },
        {
          id = 43,
          properties = {
            ["impassable"] = true
          },
          terrain = { 0, -1, 0, 0 }
        },
        {
          id = 44,
          properties = {
            ["impassable"] = true
          },
          terrain = { -1, 0, 0, 0 }
        },
        {
          id = 45,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 46,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 47,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 50,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 51,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 52,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 53,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 54,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 55,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 58,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 59,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 60,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 61,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 62,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 63,
          properties = {
            ["impassable"] = true
          }
        }
      }
    },
    {
      name = "Roads",
      firstgid = 65,
      filename = "../tilesets/city_roads.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 25,
      image = "../images/Street.png",
      imagewidth = 800,
      imageheight = 608,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {
        {
          name = "Dark Sidewalk",
          tile = 433,
          properties = {}
        },
        {
          name = "Light Sidewalk",
          tile = 436,
          properties = {}
        }
      },
      tilecount = 475,
      tiles = {
        {
          id = 11,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 12,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 86,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 87,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 224,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 226,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 249,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 274,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 299,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 303,
          terrain = { -1, 1, 1, 1 }
        },
        {
          id = 311,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 312,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 320,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 321,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 324,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 328,
          terrain = { 1, -1, 1, 1 }
        },
        {
          id = 345,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 346,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 349,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 353,
          terrain = { 1, 1, 1, -1 }
        },
        {
          id = 363,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 368,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 369,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 370,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 371,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 374,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 378,
          terrain = { 1, 1, -1, 1 }
        },
        {
          id = 386,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 387,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 393,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 394,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 395,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 396,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 399,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 404,
          terrain = { 0, 0, 0, -1 }
        },
        {
          id = 405,
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 406,
          terrain = { 0, 0, -1, 0 }
        },
        {
          id = 407,
          terrain = { -1, -1, -1, 0 }
        },
        {
          id = 408,
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 409,
          terrain = { -1, -1, 0, -1 }
        },
        {
          id = 410,
          terrain = { -1, -1, -1, 1 }
        },
        {
          id = 411,
          terrain = { -1, -1, 1, 1 }
        },
        {
          id = 412,
          terrain = { -1, -1, 1, -1 }
        },
        {
          id = 423,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 424,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 429,
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 431,
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 432,
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 433,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 434,
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 435,
          terrain = { -1, 1, -1, 1 }
        },
        {
          id = 436,
          terrain = { 1, 1, 1, 1 }
        },
        {
          id = 437,
          properties = {
            ["impassable"] = true
          },
          terrain = { 1, -1, 1, -1 }
        },
        {
          id = 454,
          terrain = { 0, -1, 0, 0 }
        },
        {
          id = 455,
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 456,
          terrain = { -1, 0, 0, 0 }
        },
        {
          id = 457,
          terrain = { -1, 0, -1, -1 }
        },
        {
          id = 458,
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 459,
          terrain = { 0, -1, -1, -1 }
        },
        {
          id = 460,
          terrain = { -1, 1, -1, -1 }
        },
        {
          id = 461,
          terrain = { 1, 1, -1, -1 }
        },
        {
          id = 462,
          terrain = { 1, -1, -1, -1 }
        }
      }
    },
    {
      name = "Cars",
      firstgid = 540,
      filename = "../tilesets/city_cars.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 12,
      image = "../images/Cars_final.png",
      imagewidth = 384,
      imageheight = 352,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 132,
      tiles = {
        {
          id = 0,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 1,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 2,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 3,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 4,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 5,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 6,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 7,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 8,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 9,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 11,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 12,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 13,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 14,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 15,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 16,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 17,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 18,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 19,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 20,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 21,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 22,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 23,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 24,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 25,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 26,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 27,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 28,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 29,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 30,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 31,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 32,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 33,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 34,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 35,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 36,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 37,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 38,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 39,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 40,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 41,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 42,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 43,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 44,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 45,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 46,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 47,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 48,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 49,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 50,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 51,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 52,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 53,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 54,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 55,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 56,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 57,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 58,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 59,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 60,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 61,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 62,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 63,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 64,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 65,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 66,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 67,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 68,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 69,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 70,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 71,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 72,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 73,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 74,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 75,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 76,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 77,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 78,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 79,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 80,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 81,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 82,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 83,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 84,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 85,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 86,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 87,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 88,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 89,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 90,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 91,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 92,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 93,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 94,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 95,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 96,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 97,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 98,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 99,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 100,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 101,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 102,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 103,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 104,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 105,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 106,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 107,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 108,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 109,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 110,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 111,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 112,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 113,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 114,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 115,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 116,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 117,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 118,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 119,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 120,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 121,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 122,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 123,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 124,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 125,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 126,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 127,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 128,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 129,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 130,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 131,
          properties = {
            ["impassable"] = true
          }
        }
      }
    },
    {
      name = "Misc Objects",
      firstgid = 672,
      filename = "../tilesets/city_misc_objects.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/Objects.png",
      imagewidth = 256,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 64,
      tiles = {
        {
          id = 0,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 2,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 3,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 8,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 9,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 11,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 12,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 13,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 16,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 17,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 18,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 19,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 25,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 26,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 27,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 28,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 29,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 30,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 31,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 36,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 37,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 38,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 39,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 40,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 47,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 48,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 49,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 56,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 57,
          properties = {
            ["impassable"] = true
          }
        }
      }
    },
    {
      name = "Fences",
      firstgid = 736,
      filename = "../tilesets/city_fences.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/Laserfence.png",
      imagewidth = 256,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 64,
      tiles = {
        {
          id = 0,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 1,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 2,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 3,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 8,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 9,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 11,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 16,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 17,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 18,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 19,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 27,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 35,
          properties = {
            ["impassable"] = true
          }
        }
      }
    },
    {
      name = "Logos",
      firstgid = 800,
      filename = "../tilesets/city_logos.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 4,
      image = "../images/Logos.png",
      imagewidth = 128,
      imageheight = 128,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 16,
      tiles = {}
    },
    {
      name = "Interior furniture",
      firstgid = 816,
      filename = "../tilesets/Interior furniture.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 16,
      image = "../images/Interior-Furniture.png",
      imagewidth = 512,
      imageheight = 512,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 256,
      tiles = {
        {
          id = 0,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 1,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 2,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 3,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 4,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 7,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 8,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 9,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 11,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 12,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 13,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 14,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 16,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 17,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 18,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 19,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 20,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 23,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 24,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 25,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 26,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 27,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 28,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 29,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 30,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 32,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 33,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 34,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 36,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 48,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 50,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 52,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 53,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 107,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 108,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 109,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 110,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 123,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 124,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 125,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 126,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 141,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 142,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 150,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 151,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 157,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 158,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 166,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 167,
          properties = {
            ["impassable"] = true
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      id = 14,
      name = "ground",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztm1tu40YQRWfQ2WN2kCyJpP75XOU4CQ/msEb2QJaBBqL7cSGZjybdt95V+v7Ht2/fgyAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgv899vYf1jfMbxjfsLxhe8P0hr8ewHiudXvDcK6x63M6n7Ge1/5zzXH+fZzPPc7jnOPdtvNe/uYd+TzO5/IO+3k9/xPn/FyO9eagJ+B7PvdlL3v+CP/wyFrwvZxrwqefOeo+ZOemNeBu1Tsd5RrLGHK76zqOcXwpct6bg55YpE836Sk8Pqr/28nJLP3iGduJUdcM5/lF93DNpPdiXWSFd+TeXc/D9lgO1vPvUets52dvDnqCPUYP2Xf28hH+scfW/036O4szbAPXDDo/62/ej/XG9tNHWE69FvKDrMA58s053qE3Bz2BfuBft3a145/x/8gRtnzU3q+Sh6V8txxYn9HlVTwOut/2g+OH7nV8wzmwhP9/MbSrbmITHuEfmYG7Rfs+aM+xCfhwdJdPy8Om61hv0Oeu66zbi+5Drhz/YWv2F+d/ald/yD6yl4/wb/12rA+HxF7Y77FdY0Cfn9pVb9F57IVjwEnHnVOsuobn827YiVfnH9vsPMy29RH+2Vdz5LgdPrAxxIaWm63cg47P7Wrr7SuQpb18t+8nrnFMgO3ozUFPsBeOAdGt/UH+senIgO29eXaMwbPJ+e3DLSPYEmRh0Wf1+ZY7dB47Azh3vDj/xMHk1dWXLvrOfi1lH2fdP2q9uawx6zzczuUT22++Zj2HtdHrQc8mJ3BMC4gdXGciTu3NQW/+vX+r9gsdxK5jF6pNJ4aDF9sT67Zz+k3Hh3KMNY5yj2s3g76b+6ld7QCf9j81ruzNQU9Y/1wjwb66hgf3jrNdW7MuIiPcxzXOB+EKndy0BvbCtV+us4+YdR7527We85laf0B2e3PQE/azjvvgwbmW82/0xzro2pz1a9czkB302nE48QNr1O+T1qoxBNfsd9bi/2EtZAF57s1BT1Ajsf92zY695RN9Zf+sf/YJ1nXHla67OVe0jjtWwPZYLrH1Vf95d9ebkE3XI2zjXj3+Z0/n8nnTvqJLrgtZVhw7wjF77fiLXM41XdfysC9e07UC2wreDblwzMDz7Pdtu3gv7unNQW/9X9tVT/b2q3+1j3U86LzR8de9eM+yhD66P8M7cMw5gmUA2ajv4x6fcxj3om7l/PTi/C/tV/9sn22fic20rrK/cOAYwjUX77ljC+eQlpUa99tHwCsy5L41Nmwt93ON81X8T28Oeuu//X31pc6bzcMucL1107GCe3nW99rPtQ47jp/bfVtQ+4PIjf08Ns3xJJ+x/9fauGsr8GDu4dH1N/dh0F3XaeFx13pru8ZnlrO1rLPdWR+4Xl1rP/x909+2Ebue25uDnjAnNY+zfo7takPhHd3DH7snz1rEeKO+4x8sP67V1/xx1Lrmj/d3TjHoubyDr3Xc8er5P/vquprtuHMuzwjaT2DnJ10PH8Tg9hMA/bbPd93edQn7e+cYljnXI7nfdQv3p11/7M1Bb/23vtW6q3N8z03Yly46b38xC47/vA52B799b2bQvT/HDrzz0a4+yTEh19X5L8t9bw56849fdy8YewknruHavmOb7UdcI3Z/37YZvXdN2XaIc44NkCXnpp7vwCa51uwY0v1D14p6c9ATrqe7rsbe/fkE3KO3XbZv9ry45QJ+kJuaG9qvO6d3P9N9aMcAPBd56M1BT2B/rX/O15/hf9Tazuc8t+P+46Hzrttge9y333SN/cTUrjHG3n61CTede/X6j/2k9xgde4Z/dNr77nq9vzuGQ/ft6z0b4LoCOu+6g+fOa016KM999fqPfarjefTjGf6r3jnXr8fc23VN171ifAAy5ZpAtQmuAzvvW3UffqI3Bz1R+37O0Zcv4N+67tjcObpnjZA71wGcH7h+WHM+fIfzCscC5A+D1nr1+A//6RqNY+ln+HcvwfU62xf3lmovd7hz3vE8cuT6g+tJc3mO6wr8r6/u/2uv1br0rP1nj2sNwDMnns/2nKB7AJ4v+vsd3D449zv05qC3/td8yz3gZ/ivPaOhfDrGd9yPPXI+h96/x+Ec/j8Fx3vYX/PwrP9nzXs5/aHneRbMOYjz0+0DDj+yDeH/9/zXOhl7/6z/dy7m2Tz7GPtm1yKA51Fi/78WnvPwbPxX1P/w80O71oeHds01PY/rPqP7tfiN9zg8wv+n4HmLo+BZ/Xdv3rma6wC2Ecic+46eN/zI/kf/PwfiK89vur7+DP/Ou11jwuZwzLOF5AyO+1w/iP5/Lcay9+7vfUX9nzjP/Ra+Ozf0jLBrUEO7xofvcTiG/0/rv2fmDv39bP3ffT///hoZ8PwPfWD3+eo87/QEx+H/PqjDex7a/Tn37Dz3ic0mpqv1Nc9vw7vndakJuOfv3wYQG7C+fwPC+9W5I69FHHm78zxijNT/fnKHnzVH3kPPBqGr2G6v4RjCvVnP3LK+dd91fs+HOT/wjCjPREadI3hmxPPjNc/BFvXmoCes8+ivZ2vcd3PNzj085+me2fTst3N6uPNv+vZ2lSNyQM+Xunfg3xV4jsiyOrerzFHrxreRg/bmoCdcY/MemTfPajmfc/zumS37iEP3+3ckcLXqOZ7fhX/HjnO7yqDridgrx5nVhth3zPrem4Pe+u/5f3N46Jh10j4UHUMOpnaN3Ti+lfN1ttt5KDrtnNT6PbarPefdrf/IkGdP7BN8rDcHPWF/aT8Jr56phRPP21ovrbuuK6Or2Iuq887/Fz1zbVc7Xde0rBGr1PlkZNNxiGvSr87/2q6+mf1yvsa+4p/NiWdzaryNfWYdz19sOgYn2AB49+zQquODzm/tGjP4ua43INO+J/p/1Q/rEf7Rvwm7lb32TJ7t/1TWw067FlB7u/ht5w62E57zxe6MeoaPu7ZI/OjfkhGL8F69OeiJOi8Jb47lrdd8ehbHNXx4gSv7EWKJoazvWNHzvo4BNj27zghhQxyn1DlmbIb73bxTbw56otbe7Cc9N8lee0bY+YJneGv87t+YuP5T+/73ZGAr9/rd7P+5xvVm2wbkwDLJ37056AnXceDY/n/TccdY6KdlxnOZ9Tcl9r++b9Qx7IZzAdcG/TsA95Fch/Dz3DtgrVVr8X/05qAn4AS+3Jup89LO+x1vOe5GXlZdM+i7/UKd8ybud0+afMH66uuI4fEPjivcy3L/2cdePf4LgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiB4FfwA6UGnlg=="
    },
    {
      type = "tilelayer",
      id = 15,
      name = "outsideGround",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt1jlKBFEUQNFvRYLTmtRdqLtQF+u0BQOn2G/QSUNn2gXec+DBDx9c6lFjAAAAAAAAAAAAAAAAAAAAAAAAAFDwcDDG45zz+b7YmstVN2Mf3mb71zlX8329NTerbsY+/PR/n3M733dbc7/qZuzDpr/737Tp7/43HS1jHC/uf9Xh4v+/zP1vc//bnua3/7xjXg7W3o6/9jEbf+6YL/3/Pf3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G/Tv03/Nv3b9G87WcY43TFny9rbAQAAAAAAAAAAAAAAAAAAAAAAAMDv+AbV2FLI"
    },
    {
      type = "tilelayer",
      id = 16,
      name = "outsideGroundStairs2",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0cEJACAMBMGQFvyl/0J9iCWoSGbg/gcbAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPys8vUDbhu5Vql/R/r3pn9v+vemP5v+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACcNgEiugIj"
    },
    {
      type = "tilelayer",
      id = 17,
      name = "outsideGroundStairs",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt18ENwkAQA0CXRqkhqQJIGaQfXAWr6GYk/y35cXsJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAd/ZNHlcz3YP/enXzd/NpTvsvZ+vmz2ZvDvsDwBK2pu9/+v7nmK3CgN7/6f2f3v85h7swp///XNMlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAl/ACb4xCz"
    },
    {
      type = "tilelayer",
      id = 18,
      name = "outsideCars",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0bcNAlEARMHTlYLNsBm2MWyGzbDdsg2QQvBnpJevtFUFAAAAAAAAAAAAAAAAAAAAAAAAQElWdVWt0yZt0y7t63+v4lcO+fqYTumcLunq/2Lc8vU9PdIzvdLb/8Vo5OtmaqV26qSu/4vRy9f9NEjDNEpj/xdjkq+naZbmaZGW/gcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4IsP8AsNbw=="
    },
    {
      type = "tilelayer",
      id = 19,
      name = "outsideProps",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt3M1KAzEUBtAhff936MKCC/+WLrSLVoXqMwjiSujGRzBFhglDniDfOXAZsv46uemFzDQBAAAAAAAAAAAAAAAAAAAAAAAAAAAwusfy/zzU57Esa3Lc1czvy/IbAMa29Z5Hu5Z/tAf5A8T6rj3gq1NkONesfzpFhkv+v50iw1nW0Xr5mwvl6OVvLpSjl7+5UA79P5v+n03/z6b/Z9P/s2025v/JerN/8/8cvdm/+T/A2A5l2rbr97rvf9j7YxxX+QMAkOfk/B/L/z/I1O77J3MBGN78zs9z4Ke6fq61dwaIcpkDf9bMX2q91nqTf4xdk7XvfuW5lTlAnPmez3rmu9MTIrT3fNrvfjoTZFjf87kqS5HnpiwFAAAAAAAAAAAAAAAAAAAAAAAAACP4Ayv9Ufg="
    },
    {
      type = "tilelayer",
      id = 20,
      name = "outsideProps2",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztzjENAAAIA7AF/3bwhwUewtMqaAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABc6foeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADA3gA3YQC4"
    },
    {
      type = "tilelayer",
      id = 21,
      name = "outsidePropsRedLights",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt2EENACAQBDGCf3k8EYMMCNMquOQynx0DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIDfrXn7Am7a/p+m/zb9t+m/Tf9t+m/TPwAAAAD8y/7bZv9t03+b/tv036b/Nv236R8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4wQEgHw2h"
    },
    {
      type = "tilelayer",
      id = 22,
      name = "walls",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt1ctKw1AYhdGYgiC+QXWkjnToreAFZzryEcUn8A0FT+blmNqYNtlrwaaTonI+fmwaAAAAAAAAAAAAAAAAAAAAAAAAAGiag7J2xlsM91SzdNTzex8DPOTLmv234xF+x5Tpn23M/ndl12U3Zbdlq+1/5K/0rxu7/0PZY9lTo/8+GLP/Luhf17f/l/6z1Lf/VOlf1/Uvt/35sWa7/tuGoH+d+8+mfzb9s+mfTf9s+mfTP5v+2fTPpn82/bPpn63r37b9NkX613X9l22/TZH+dV3/77bfpkj/Ov//s+mfTf9sf+1/uGia+7LVBp+7oH/dNv1fy942+NwF/eu26b+JZ/330lj93/XfS+4/m/vP5v6zuf9s7j+b+892Wrac8U6Ge6pZuig7m/HOh3uqWdI/m/7Z9M+mfzb9s+mfTf9s+mfTP5v+2br+lzPe1XBPBQAAAAAAAAAAAAAAAAAAAAAAAMAe+wEOPYn4"
    },
    {
      type = "tilelayer",
      id = 23,
      name = "props",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt2z1KA0EYBuBl5zIKCv6ksBJ/UqQKKh5ARO/hPUylnbFSKz2FpbdxJC5Zl7hMyCbG7PPARybMLCy8mRlSfFkGAAAAAAAAAAAAAAAAAAAAAAAAAGOXYTw+Dz/nLirfWW1X8m6trZj9dqwdv4FWOoi5H8Y6kn8rncTcT2OdyR8AAAAAAAAAAP6FuzzL7vPR+CV+PsV6zv/2nVich5j1sJT3axy/yR8AgAluQ1qxmoYhrVLs16zbq5mLz91M9dI0psn93//uO+uU1u+GUU9ar2auL/+ZvCf816/2hBea3P9F/t3S+uNK/pPm5D+bj4T8f+sJH4S0SlHkX1XOf9Kc/Ofn64wt94R3Khk8hrRK0a9Z16uZk//8FGds0RPereQwj/3v/l8udT3hTe9/9/9yuk7McBbuf4DVtRbP6/UF3CUsp42Y/ab8AQAAAAAAAAAAAAAAAAAAAAAAAAAAAACAKXwCdzhalQ=="
    },
    {
      type = "objectgroup",
      id = 12,
      name = "playerSpawn",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 3,
          name = "player",
          type = "",
          shape = "rectangle",
          x = 2210.94,
          y = 1790.86,
          width = 16.6667,
          height = 15,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 13,
      name = "enemySpawn",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 4,
          name = "",
          type = "",
          shape = "rectangle",
          x = 2318,
          y = 2417,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 3030,
          y = 2408,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 3030,
          y = 2020,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "",
          type = "",
          shape = "rectangle",
          x = 3024,
          y = 1648,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 8,
          name = "",
          type = "",
          shape = "rectangle",
          x = 3026,
          y = 1318,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 9,
          name = "",
          type = "",
          shape = "rectangle",
          x = 2530,
          y = 1310,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 10,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1892,
          y = 1316,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 12,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1368.67,
          y = 1565.33,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 13,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1364,
          y = 1846,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 14,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1398,
          y = 2348,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
