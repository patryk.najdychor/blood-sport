pilcrow.weapons.Shotgun = pilcrow.weapons.Weapon:extend()

function pilcrow.weapons.Shotgun.constructor(owner)
  local weaponBuilder = pilcrow.weapons.WeaponBuilder()
  weaponBuilder:setOwner(owner)

  weaponBuilder:setAutomatic(false)
  weaponBuilder:setBulletSpeed(280)
  weaponBuilder:setDamage(11)
  weaponBuilder:setSecondsPerShot(1.2)
  weaponBuilder:setSound(love.audio.newSource("/assets/sounds/Laser Shot_1_single.wav", "static"))
  weaponBuilder:setRange(250)

  return {{weaponBuilder}}
end

function pilcrow.weapons.Shotgun:new(protected, owner)
  local bulletSpeedLevel = 1
  local damageLevel = 1
  local secondsPerShotLevel = 1
  local rangeLevel = 1

  protected.bullet = pilcrow.weapons.Bullet

  --[[------------------------------------------------
  PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:shoot(startX, startY, directionX, directionY)
    if not self:canShoot() then return end

    local dir = math.atan2(( directionY - startY ), ( directionX - startX ))

    local bulletAmount = 10
    for i=0, bulletAmount do
      local targetX = directionX + ((i - (bulletAmount * 0.5)) * 10 * math.cos(dir))
      local targetY = directionY + ((i - (bulletAmount * 0.5)) * 10 * math.sin(dir))
      protected.newBullet(startX, startY, targetX, targetY)
    end

    love.audio.stop(self:getSound())
    love.audio.play(self:getSound())

    protected.lastShotTime = 0
    protected.wasMouseReleased = false
  end

  function self:upgradeBulletSpeed()
    if bulletSpeedLevel <= 10 then
      protected.bulletSpeed = self:getBulletSpeed() * 1.1
      bulletSpeedLevel = bulletSpeedLevel + 1
      return true
    end
    return false
  end

  function self:upgradeDamage()
    if damageLevel <= 10 then
      protected.damage = self:getDamage() * 1.1
      damageLevel = damageLevel + 1
      return true
    end
    return false
  end

  function self:upgradeRange()
    if rangeLevel <= 10 then
      protected.range = self:getRange() * 1.1
      rangeLevel = rangeLevel + 1
      return true
    end
    return false
  end

  function self:upgradeSecondsPerShot()
    if secondsPerShotLevel <= 10 then
      protected.secondsPerShot = self:getSecondsPerShot() * 0.8
      secondsPerShotLevel = secondsPerShotLevel + 1
      return true
    end
    return false
  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.weapons.Shotgun:__tostring()
  return "pilcrow.weapons.Shotgun"
end
