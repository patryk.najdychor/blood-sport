pilcrow.weapons.WeaponBuilder = pilcrow.Object:extend()

function pilcrow.weapons.WeaponBuilder:new()
  local automatic
  local bulletSpeed
  local damage
  local owner
  local secondsPerShot
  local sound
  local range

  local blastRadius

  local errorMessage = "Tried to set invalid pilcrow.weapons.WeaponBuilder parameter "

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:getAutomatic()
    return automatic
  end

  function self:getBlastRadius()
    return blastRadius
  end

  function self:getBulletSpeed()
    return bulletSpeed
  end

  function self:getDamage()
    return damage
  end

  function self:getOwner()
    return owner
  end

  function self:getRange()
    return range
  end

  function self:getSecondsPerShot()
    return secondsPerShot
  end

  function self:getSound()
    return sound
  end

  function self:setAutomatic(v)
    if type(v) ~= "boolean" then
      error(errorMessage .. "automatic" .. tostring(v), 3)
    end

    automatic = v
    return self
  end

  function self:setBlastRadius(v)
    if v <= 0 then
      error(errorMessage .. "blastRadius" .. tostring(v), 3)
    end

    blastRadius = v
    return self
  end

  function self:setBulletSpeed(v)
    if v <= 0 then
      error(errorMessage .. "bulletSpeed" .. tostring(v), 3)
    end

    bulletSpeed = v
    return self
  end

  function self:setDamage(v)
    if v <= 0 then
      error(errorMessage .. "damage" .. tostring(v), 3)
    end

    damage = v
    return self
  end

  function self:setOwner(v)
    if not v or v < 1 then
      error(errorMessage .. "owner " .. tostring(v), 3)
    end

    owner = v
    return self
  end

  function self:setRange(v)
    if v <= 0 then
      error(errorMessage .. "range" .. tostring(v), 3)
    end

    range = v
    return self
  end

  function self:setSecondsPerShot(v)
    if v <= 0 or not v then
      error(errorMessage .. "secondsPerShot " .. tostring(v), 3)
    end

    secondsPerShot = v
    return self
  end

  function self:setSound(v)
    sound = v
    return self
  end

  function self:validateParameters()
    local missingParameters = {}

    if not owner then
      table.insert(missingParameters, "owner")
    end
    if not secondsPerShot then
      table.insert(missingParameters, "secondsPerShot")
    end
    if not bulletSpeed then
      table.insert(missingParameters, "bulletSpeed")
    end
    if not damage then
      table.insert(missingParameters, "damage")
    end
    if not sound then
      table.insert(missingParameters, "sound")
    end
    if not range then
      table.insert(missingParameters, "range")
    end
    if automatic == nil then
      table.insert(missingParameters, "automatic")
    end

    if #missingParameters > 0 then
      local errorMessage = "WeaponBuilder failed, missing parameters"
      for k,v in pairs(missingParameters) do
        errorMessage = errorMessage .. " " .. v
      end
      error(errorMessage, 4)
    end
  end
end

--[[------------------------------------------------
  STATIC FUNCTIONS
]]--------------------------------------------------

function pilcrow.weapons.WeaponBuilder.__tostring()
  return "pilcrow.weapons.WeaponBuilder"
end
