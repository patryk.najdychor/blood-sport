pilcrow.weapons = {}

require("classes/weapons/bullets/Bullet")
require("classes/weapons/bullets/MeleeBullet")
require("classes/weapons/bullets/PenetratingBullet")
require("classes/weapons/bullets/GrenadeBullet")
require("classes/weapons/bullets/TracingBullet")

require("classes/weapons/abstract/Weapon")
require("classes/weapons/WeaponBuilder")

require("classes/weapons/Pistol")
require("classes/weapons/Smg")
require("classes/weapons/Shotgun")
require("classes/weapons/GrenadeLauncher")
require("classes/weapons/Katana")
