pilcrow.weapons.PenetratingBullet = pilcrow.weapons.Bullet:extend()

function pilcrow.weapons.PenetratingBullet.constructor(startX, startY, endX, endY, speed, damage, range, owner)
  return {{startX, startY, endX, endY, speed, damage, range, owner}}
end

function pilcrow.weapons.PenetratingBullet:new(protected, startX, startY, endX, endY, speed, damage, range, owner)
  --we keep a separate targetsHit counter instead of using #targetsHitObjects
  --because this is a table indexed by strings
  local targetsHit = 0
  local targetsHitObjects = {}
  local maxTargetsHit = 3

  --[[------------------------------------------------
    PROTECTED FUNCTIONS
  ]]--------------------------------------------------

  function protected.tryHit()
    local objects = protected.getHitTargets()

    for k, object in pairs(objects) do
      if not targetsHitObjects[object] and protected.doesHit(object) then
        --FIXME pass raw damage to hit object
        object:hit(self)
        targetsHit = targetsHit + 1
        targetsHitObjects[object] = true
        if targetsHit >= maxTargetsHit then
          protected.markForRemoval = true
          return
        end
      end
    end
  end
end
