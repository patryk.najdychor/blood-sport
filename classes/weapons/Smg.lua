pilcrow.weapons.Smg = pilcrow.weapons.Weapon:extend()

function pilcrow.weapons.Smg.constructor(owner)
  local weaponBuilder = pilcrow.weapons.WeaponBuilder()
  weaponBuilder:setOwner(owner)

  weaponBuilder:setAutomatic(true)
  weaponBuilder:setBulletSpeed(350)
  weaponBuilder:setDamage(18)
  weaponBuilder:setSecondsPerShot(0.15)
  weaponBuilder:setSound(love.audio.newSource("/assets/sounds/weapons/smg.mp3", "static"))
  weaponBuilder:setRange(300)

  return {{weaponBuilder}}
end

function pilcrow.weapons.Smg:new(protected, owner)
  local bulletSpeedLevel = 1
  local damageLevel = 1
  local secondsPerShotLevel = 1
  local rangeLevel = 1

  --[[------------------------------------------------
  PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:upgradeBulletSpeed()
    if bulletSpeedLevel <= 10 then
      protected.bulletSpeed = self:getBulletSpeed() * 1.1
      bulletSpeedLevel = bulletSpeedLevel + 1
      return true
    end
    return false
  end

  function self:upgradeDamage()
    if damageLevel <= 10 then
      protected.damage = self:getDamage() * 1.1
      damageLevel = damageLevel + 1
      return true
    end
    return false
  end

  function self:upgradeRange()
    if rangeLevel <= 10 then
      protected.range = self:getRange() * 1.1
      rangeLevel = rangeLevel + 1
      return true
    end
    return false
  end

  function self:upgradeSecondsPerShot()
    if secondsPerShotLevel <= 10 then
      protected.secondsPerShot = self:getSecondsPerShot() * 0.8
      secondsPerShotLevel = secondsPerShotLevel + 1
      return true
    end
    return false
  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.weapons.Smg:__tostring()
  return "pilcrow.weapons.Smg"
end
