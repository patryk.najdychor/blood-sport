pilcrow.weapons.GrenadeLauncher = pilcrow.weapons.Weapon:extend()

function pilcrow.weapons.GrenadeLauncher.constructor(owner, _)
  local weaponBuilder = pilcrow.weapons.WeaponBuilder()
  weaponBuilder:setOwner(owner)

  weaponBuilder:setAutomatic(false)
  weaponBuilder:setBulletSpeed(150)
  weaponBuilder:setDamage(300)
  weaponBuilder:setSecondsPerShot(2.5)
  weaponBuilder:setSound(love.audio.newSource("/assets/sounds/grenadeShot.mp3", "static"))
  weaponBuilder:setRange(200)

  return {{weaponBuilder}}
end

function pilcrow.weapons.GrenadeLauncher:new(protected, owner)
  local owner = owner
  local blastRadius = 50

  local blastRadiusLevel = 1
  local bulletSpeedLevel = 1
  local damageLevel = 1
  local secondsPerShotLevel = 1
  local rangeLevel = 1

  protected.bullet = pilcrow.weapons.GrenadeBullet

  --[[------------------------------------------------
    PROTECTED FUNCTIONS
  ]]--------------------------------------------------

  function protected.newBullet(startX, startY, directionX, directionY)
    return protected.bullet(blastRadius, startX, startY, directionX, directionY, protected.bulletSpeed, protected.damage, protected.range, owner)
  end

  --[[------------------------------------------------
  PUBLIC FUNCTIONS
  ]]--------------------------------------------------


  function self:upgradeBlastRadius()
    if blastRadiusLevel <= 10 then
      protected.blastRadius = self:getBlastRadius() * 1.1
      blastRadiusLevel = blastRadiusLevel + 1
      return true
    end
    return false
  end

  function self:upgradeBulletSpeed()
    if bulletSpeedLevel <= 10 then
      protected.bulletSpeed = self:getBulletSpeed() * 1.1
      bulletSpeedLevel = bulletSpeedLevel + 1
      return true
    end
    return false
  end

  function self:upgradeDamage()
    if damageLevel <= 10 then
      protected.damage = self:getDamage() * 1.1
      damageLevel = damageLevel + 1
      return true
    end
    return false
  end

  function self:upgradeRange()
    if rangeLevel <= 10 then
      protected.range = self:getRange() * 1.1
      rangeLevel = rangeLevel + 1
      return true
    end
    return false
  end

  function self:upgradeSecondsPerShot()
    if secondsPerShotLevel <= 10 then
      protected.secondsPerShot = self:getSecondsPerShot() * 0.8
      secondsPerShotLevel = secondsPerShotLevel + 1
      return true
    end
    return false
  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.weapons.GrenadeLauncher:__tostring()
  return "pilcrow.weapons.GrenadeLauncher"
end
