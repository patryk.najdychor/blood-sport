pilcrow.Screens.ShopItem = pilcrow.Object:extend()

function pilcrow.Screens.ShopItem:new(protected, type, name, price, level, index, onClick)
     local type = type
     local name = name
     local nameString = "Name: " .. name
     local price = price
     local priceString = "Price: " .. price
     local level = level
     local levelString = "Level: " .. level
     local index = index
     local margin = love.graphics.getWidth()*0.008
     local width = (((love.graphics.getWidth() * 0.7) - 25) * 0.8 - (17 * 2)) - (margin * 2)
     local height = love.graphics.getWidth()*0.055
     local x = (((love.graphics.getWidth() * 0.7) - 25) * 0.1 ) * 2 + (17 * 2) + margin
     local y = (love.graphics.getHeight() * 0.315) + (margin * index) + (height * (index - 1))

     local player = pilcrow.GameHelper.getInstance():getPlayer()

     function self:draw()
          love.graphics.setColor(1, 1, 1)
          if isOpen == false then return end
          if type == 1 then
               love.graphics.rectangle("fill", x, y, width, height)
               self:drawInfo(false)
          elseif type == 0 then
               love.graphics.rectangle("fill", x, y, width, height)
               self:drawInfo(true)
          end
     end

     function self:deactivate()
          isOpen = false
     end

     function self:onClick()

     end

     function self:drawInfo(drawLevel)
          love.graphics.setColor(0, 0, 0)
          local font = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.02)
          local nameToDraw = love.graphics.newText(font, nameString)
          local priceToDraw = love.graphics.newText(font, priceString)
          local levelToDraw = love.graphics.newText(font, levelString)
          love.graphics.draw(nameToDraw, x + margin, y + (love.graphics.getHeight() * 0.0007))
          love.graphics.draw(priceToDraw, x + margin, y + (love.graphics.getHeight() * 0.0007) + nameToDraw:getHeight())
          if drawLevel then
               love.graphics.draw(levelToDraw, x + (margin * 5) + (width / 2), y + (love.graphics.getHeight() * 0.0007))
               for i = 0 , 9 do
                    love.graphics.setColor(0, 1, 0)
                    if i + 1 > level then
                         love.graphics.setColor(0, 0, 0)
                    end
                    love.graphics.rectangle("fill", x + (margin * 5) + (32 * i) + (width / 2), y + (love.graphics.getHeight() * 0.0007) + nameToDraw:getHeight(), 30, 30)
               end
          end
     end

     function self:isHovered()
          local mouseX, mouseY = love.mouse.getPosition()
          if mouseX > x and mouseX < (x + width) and mouseY > y and mouseY < (y + height) then
               return true
          else
               return false
          end
     end

     function self:getPrice()
          return price
     end

     if onClick then
          self.onClick = onClick
     end

end
pilcrow.Screens.ShopItem.SHOP_ITEM_TYPES = {}
pilcrow.Screens.ShopItem.SHOP_ITEM_TYPES.UPGRADE = 0
pilcrow.Screens.ShopItem.SHOP_ITEM_TYPES.WEAPON = 1
