--[[------------------------------------------------
  ABSTRACT CLASS
]]--------------------------------------------------
Screen = Object:extend()

function Screen:new()
  error("Tried to instantiate an abstract Screen class", 2)
end

function Screen:draw() end

function Screen:handle(input)
  if not self.bindings then return end

  local action = self.bindings[input] or self.bindings.any
  if action then
    return action()
  end
end

function Screen:handleReleased(input)
  if not self.bindingsReleased then return end

  local action = self.bindingsReleased[input] or self.bindingsReleased.any
  if action then
    return action()
  end
end

function Screen:keyPressed(key, scancode, isrepeat)
  if not self.keys and not (self.bindings and self.bindings.any) then return end

  local binding = self.keys[scancode]
  return self:handle(binding)
end

function Screen:keyReleased(key, scancode)
  if not self.keys and not (self.bindingsReleased and self.bindingsReleased.any) then return end

  local binding = self.keys[scancode]
  return self:handleReleased(binding)
end

function Screen:load() end

function Screen:finalize() end

function Screen:mousePressed() end

function Screen:mouseReleased() end

function Screen:update(dt) end
