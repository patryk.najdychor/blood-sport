pilcrow.screens.PauseGameComponent = pilcrow.Object:extend()

function pilcrow.screens.PauseGameComponent:new(protected, gameScreen)
  local gameScreen = gameScreen
  local menuFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.025)
  local window

  --holds original Game screen functions when pause() runs
  local drawFunctionBackup
  local mouseReleasedFunctionBackup
  local updateFunctionBackup

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:draw()
    love.graphics.setBackgroundColor(1, 1, 1, 1)
    love.graphics.setColor(0, 0, 0)
    if window then window:draw() end
  end

  function self:mouseReleased(x, y, button, isTouch)
    if window then
      for i, textBox in ipairs(window:getTextBoxes()) do
        if textBox:isHovered() then
          textBox:onClick()
          return
        end
      end
    end
  end

  function self:unPause()
    --restore original Game screen functions
    gameScreen.draw = drawFunctionBackup
    gameScreen.mouseReleased = mouseReleasedFunctionBackup
    gameScreen.update = updateFunctionBackup
  end

  window = Window(love.graphics.getWidth() * 0.5, love.graphics.getHeight() * 0.5)
  window:addTextBox(TextBox(menuFont, pilcrow.Language:getText("continue"), function() gameScreen:unPause() end))
  window:addTextBox(TextBox(menuFont, pilcrow.Language:getText("exit"), function() love.event.quit(0) end))
  window:setTextBoxesColor(1,1,1,1)

  --save original Game screen functions to restore on unpause
  drawFunctionBackup = gameScreen.draw
  mouseReleasedFunctionBackup = gameScreen.mouseReleased
  updateFunctionBackup = gameScreen.update
  gameScreen.draw = self.draw
  gameScreen.mouseReleased = self.mouseReleased
  gameScreen.update = self.update
end
