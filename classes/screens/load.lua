pilcrow = pilcrow or {}

--FIXME package names should be lowercase
pilcrow.Screens = pilcrow.Screens or {}
pilcrow.State = pilcrow.State or {}

pilcrow.screens = {}

require("classes/screens/Screen")
require("classes/screens/MainMenu")
require("classes/screens/Game")
require("classes/screens/PauseGameComponent")
require("classes/screens/GameOver")
require("classes/screens/Shop")
require("classes/screens/shopFiles/ShopSection")
require("classes/screens/shopFiles/ShopGui")
require("classes/screens/shopFiles/ShopItem")