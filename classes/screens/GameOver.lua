local GameOver = Screen:extend()

function GameOver:new()

  self.bindings = {
    any = function() pilcrow.changeGameScreen("MainMenu") end
  }

  self.bindingsReleased = {
    any = function() pilcrow.changeGameScreen("MainMenu") end
  }

  self.keys = {
  }

  local textBoxes = {}
  local timeOnScreen = 0

  local titleFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.04)
  local menuFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.025)

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:draw()
    love.graphics.setBackgroundColor(1, 0, 0, 1)
    love.graphics.setColor(0, 0, 0)

    for i, textBox in ipairs(textBoxes) do
      local y = love.graphics.getHeight() * 0.2
      y = y + (love.graphics.getHeight() * 0.1 * i)
      textBox:draw(love.graphics.getWidth() * 0.1, y)
    end
  end

  function self:finalize()
    love.audio.stop()
  end

  function self:update(dt)
    timeOnScreen = timeOnScreen + dt
    if timeOnScreen > 3 then
      self.handle = Screen.handle
      self.handleReleased = Screen.handleReleased
    end
  end

  --[[------------------------------------------------
    CONSTRUCTOR
  ]]--------------------------------------------------

  local titleTextBox = TextBox(titleFont, pilcrow.Language:getText("dead"))
  titleTextBox:setHoverable(false)
  table.insert(textBoxes, titleTextBox)

  local continueTextBox = TextBox(menuFont, pilcrow.Language:getText("pressToContinue"))
  continueTextBox:setHoverable(false)
  table.insert(textBoxes,continueTextBox)

  local sound = love.audio.newSource("/assets/sounds/GameOverScreenMusic.wav", "static")
  love.audio.play(sound)

  --disable handlers for a couple of seconds to prevent skipping the screen by player by accident
  --they are set back in self:update()
  self.handle = function() end
  self.handleReleased = function() end
end

pilcrow.Screens.GameOver = GameOver
