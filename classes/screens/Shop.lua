local Shop = Screen:extend()

function Shop:new()
     local titleFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.04)
     local menuFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.025)
     local shopGui = ShopGui()
     local textBoxes = {
          TextBox(menuFont, "Shop", function() end),
          TextBox(menuFont, "Continue", function()
               pilcrow.changeGameScreen("Game")
          end)
     }


     local shopSections = {
          ShopSection("Weapons", 1),
          ShopSection("Upgrades", 2),
          ShopSection("Power-ups" , 3)
     }

     local katanaText = pilcrow.Language:getText("katana")
     local shopItem = pilcrow.Screens.ShopItem(pilcrow.Screens.ShopItem.SHOP_ITEM_TYPES.WEAPON, katanaText, 500, 1, 1, function()
        local itemPrice = 500
        local player = pilcrow.GameHelper.getInstance():getPlayer()
        if player:canAfford(itemPrice) then
          player:addPoints(-itemPrice)
          player:setWeapon(pilcrow.weapons.Katana(pilcrow.weapons.Weapon.OWNER.PLAYER))
        end
     end)
     shopSections[1]:addShopItem(shopItem)

     local smgText = pilcrow.Language:getText("smg")
     shopItem = pilcrow.Screens.ShopItem(pilcrow.Screens.ShopItem.SHOP_ITEM_TYPES.WEAPON, smgText, 3500, 1, 1, function()
        local itemPrice = 3500
        local player = pilcrow.GameHelper.getInstance():getPlayer()
        if player:canAfford(itemPrice) then
          player:addPoints(-itemPrice)
          player:setWeapon(pilcrow.weapons.Smg(pilcrow.weapons.Weapon.OWNER.PLAYER))
        end
     end)
     shopSections[1]:addShopItem(shopItem)

     local grenadeLauncherText = pilcrow.Language:getText("grenadeLauncher")
     shopItem = pilcrow.Screens.ShopItem(pilcrow.Screens.ShopItem.SHOP_ITEM_TYPES.WEAPON, grenadeLauncherText, 10000, 1, 1, function()
        local itemPrice = 10000
        local player = pilcrow.GameHelper.getInstance():getPlayer()
        if player:canAfford(itemPrice) then
          player:addPoints(-itemPrice)
          player:setWeapon(pilcrow.weapons.GrenadeLauncher(pilcrow.weapons.Weapon.OWNER.PLAYER))
        end
     end)
     shopSections[1]:addShopItem(shopItem)

     shopItem = pilcrow.Screens.ShopItem(pilcrow.Screens.ShopItem.SHOP_ITEM_TYPES.UPGRADE, "Granade Launcher", 50, 7, 1, function()
       local itemPrice = 50
       local player = pilcrow.GameHelper.getInstance():getPlayer()
       if player:canAfford(itemPrice) then
         player:addPoints(-itemPrice)

       end
      end)
     shopSections[2]:addShopItem(shopItem)

  --[[------------------------------------------------
  PRIVATE FUNCTIONS
  ]]--------------------------------------------------

     local function shiftShopSections(indexToShift)
          if shopSections[#shopSections]:getIndex() ~= indexToShift then
               for b , shopSection in ipairs(shopSections) do
                    local index = shopSection:getIndex()
                    if index == 3 then
                         shopSection:changeIndex(1)
                    else
                         shopSection:changeIndex(index + 1)
                    end
               end
               local element = table.remove(shopSections, shopSectionsLength)
               table.insert(shopSections, 1, element)
          else
               shiftShopSections(indexToShift)
          end
     end

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

     function self:draw()
            love.graphics.setBackgroundColor(1, 1, 1, 1)
            love.graphics.setColor(0, 0, 0)
            shopGui:draw()

          for i, textBox in ipairs(textBoxes) do
               if i == 1 then
                    textBox:draw(love.graphics.getWidth() * 0.91, love.graphics.getHeight() * 0.015)
               elseif i == #textBoxes then
                    textBox:draw(love.graphics.getWidth() * 0.855, love.graphics.getHeight() * 0.9)
               else
                    local y = love.graphics.getHeight() * 0.2
                    y = y + (love.graphics.getHeight() * 0.1 * i)
                    textBox:draw(love.graphics.getWidth() * 0.1, y)
               end
          end

          for i, shopSection in ipairs(shopSections) do
               local index = shopSection:getIndex()
               local width = ((love.graphics.getWidth() * 0.7) - 25) * 0.1
               local borderWidth = shopSection:getBorderWidth()

               if index == 1 then
                    shopSection:update(0, width)
                    shopSection:draw()
               elseif index == 2 then
                    shopSection:update(width + borderWidth, width)
                    shopSection:draw()
               elseif index == 3 then
                    shopSection:update(width * 2 + borderWidth * 2, ((love.graphics.getWidth() * 0.7) - 25) * 0.8 - borderWidth * 2)
                    shopSection:draw()
               end
          end
     end

     function self:mousePressed(x, y, button, isTouch)
          if button ~= 1 then return end

          for i,textBox in ipairs(textBoxes) do
               if textBox:isHovered() then
                    textBox:onClick()
                    return
               end
          end

          for a,shopSection in ipairs(shopSections) do
               if  shopSection:getIndex() ~= 3 then
                    if shopSection:isHovered() then
                         shiftShopSections(shopSection:getIndex())
                    end
               elseif shopSection:getIndex() == 3 then
                    shopSection:clickItem()
               end
          end
     end


end
pilcrow.Screens.Shop = Shop
