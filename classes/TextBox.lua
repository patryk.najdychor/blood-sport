TextBox = Object:extend()

function TextBox:new(font, text, onClick)
  self.text = text
  self.textObject = love.graphics.newText(font, text)
  self.w = self.textObject:getWidth()
  self.h = self.textObject:getHeight()
  self.onClick = onClick
  self.visible = true
  self.hoverable = true
  self.r = 0
  self.g = 0
  self.b = 0
  self.a = 1
  self.x = 0
  self.y = 0
end

function TextBox:draw(x, y)
  self.x = x
  self.y = y
  if not self:isVisible() then return end

  if self:isHovered() or (self.parentWindow and self.parentWindow:getHoveredTextBox() == self) then
    love.graphics.setColor(1, 0, 0)
  else
    love.graphics.setColor(self.r, self.g, self.b, self.a)
  end

  love.graphics.draw(self:getText(), self:getX(), self:getY())
end

function TextBox:getText()
  return self.textObject
end

function TextBox:getTextW()
  return self.textObject:getWidth()
end

function TextBox:getTextH()
  return self.textObject:getHeight()
end

function TextBox:getString()
  return self.text
end

function TextBox:getX()
  return self.x
end

function TextBox:getY()
  return self.y
end

function TextBox:getW()
  return self.w
end

function TextBox:getH()
  return self.h
end

function TextBox:isHoverable()
  return self.hoverable
end

function TextBox:isHovered()
  if not self:isVisible() then return false end
  if not self:isHoverable() then return false end

  local mouseX, mouseY = love.mouse.getPosition()
  if mouseX > self.x and mouseX < (self.x + self.w) and mouseY > self.y and mouseY < (self.y + self.h) then
    return true
  else
    return false
  end
end

function TextBox:isVisible()
  return self.visible
end

function TextBox:onClick()

end

function TextBox:setColor(r, g, b, a)
  self.r = r
  self.g = g
  self.b = b
  self.a = a
end

function TextBox:setHovered(bool)
  self.isHovered = bool
end

function TextBox:setHoverable(hoverable)
  self.hoverable = hoverable
end

function TextBox:setParentWindow(parentWindow)
  self.parentWindow = parentWindow
end

function TextBox:setString(text)
  self.text = text
  self.textObject:clear()
  self.textObject:set(text)
end

function TextBox:setVisible(visible)
  self.isVisible = visible
end
