local texts = {}

--MainMenu screen
texts.newGame = "Neues Spiel"
texts.settings = "Die Einstellungen"
texts.credits = "Abspann"
texts.exit = "Ausgang"
texts.enableFullscreen = "Vollbild Aktivieren"
texts.disableFullscreen = "Vollbild Deaktivieren"
texts.changeResolution = "Ändern Sie Die Auflösung"
texts.back = "Zurück"
texts.programmers = "Programmierer"
texts.audioDesign = "Audio Design"
--PauseMenu screen
texts.resume = "Fortsetzen"
--GameOver Screen
texts.dead = "Du bist tot!"
texts.pressToContinue = "Drücken Sie eine beliebige Taste, um fortzufahren"
--Shop screen
texts.shop = "Geschäft"
texts.continue = "Fortsetzen"
texts.buySmg = "SMG kaufen"
texts.upgradeBulletSpeed = "Steigern Sie die Geschwindigkeit Ihres Geschosses"
texts.upgradeDamage = "Verbessere den Schaden"
texts.upgradeFireRate = "Verbessere die Schussrate"
texts.upgradeWeaponsRange = "Verbessere die Reichweite der Waffe"


return texts
