local Protected = {}
Protected.__index = Protected

function Protected:__tostring()
  return "Protected"
end

function Protected:__call(...)
  local protected = setmetatable({}, self)
  return protected
end

return Protected
