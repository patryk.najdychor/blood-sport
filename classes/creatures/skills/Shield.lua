pilcrow.creatures.skills.Shield = pilcrow.creatures.skills.Skill:extend()

function pilcrow.creatures.skills.Shield.constructor(owner, skillTree)
  local maxLevel = 4
  return {{owner, skillTree, maxLevel}}
end

function pilcrow.creatures.skills.Shield:new(protected)
  --initially high to allow instant first use
  local secondsSinceUse = 100
  local shieldHealth = 50

  --nil if skill not in use
  local originalOwnerSubtractHealth

  function self:canUse()
    if secondsSinceUse > 10 and not originalOwnerSubtractHealth then
      return true
    end
  end

  function self:getDisplayName()
    return pilcrow.Language:getText("Shield")
  end

  function self:getName()
    return "Shield"
  end

  function self:getType()
    return pilcrow.creatures.skills.Skill.ACTIVE
  end

  function self:upgrade()
    if self:reachedLevelLimit() then return false end
    protected.levelUp()

    shieldHealth = shieldHealth + (self:getLevel() * 15)
  end

  function self:use()
    if not self:canUse() then return false end

    local owner = protected.getOwner()
    originalOwnerSubtractHealth = owner.subtractHealth

    local shield = shieldHealth
    owner.subtractHealth = function(self, amount)
      shield = shield - amount
      if shield <= 0 then
        owner.subtractHealth = originalOwnerSubtractHealth
        originalOwnerSubtractHealth = nil
      end
    end
  end

  function self:update(dt)
    --count only after shields are depleted
    if not originalOwnerSubtractHealth then
      secondsSinceUse = secondsSinceUse + dt
    end
  end
end

function pilcrow.creatures.skills.Shield.__tostring()
  return "pilcrow.creatures.skills.Shield"
end
