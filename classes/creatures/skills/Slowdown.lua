pilcrow.creatures.skills.Slowdown = pilcrow.creatures.skills.Skill:extend()

function pilcrow.creatures.skills.Slowdown.constructor(owner, skillTree)
  local maxLevel = 5
  return {{owner, skillTree, maxLevel}}
end

function pilcrow.creatures.skills.Slowdown:new(protected)
  local SLOWDOWN_RUNTIME = 5
  local SLOWDOWN_COOLDOWN = 20

  --initially high to allow instant first use
  local secondsSinceUse = 100
  local speedMultiplier = 1
  local inUse = false

  local function endUse()
    inUse = false

    local enemies = pilcrow.GameHelper.getInstance():getEnemies()
    for k, enemy in pairs(enemies) do
      if enemy.speedBeforeSlowdown then
        local enemySpeed = enemy.speedBeforeSlowdown
        enemy:setSpeed(enemySpeed)
        enemy.speedBeforeSlowdown = nil
      end
    end
  end

  function self:canUse()
    if secondsSinceUse > SLOWDOWN_COOLDOWN then
      return true
    end
  end

  function self:getDisplayName()
    return pilcrow.Language:getText("Slowdown")
  end

  function self:getName()
    return "Slowdown"
  end

  function self:getType()
    return pilcrow.creatures.skills.Skill.ACTIVE
  end

  function self:upgrade()
    if self:reachedLevelLimit() then return false end
    protected.levelUp()

    speedMultiplier = self:getLevel() * 0.04
  end

  function self:use()
    if not self:canUse() then return false end
    if inUse then return false end

    local enemies = pilcrow.GameHelper.getInstance():getEnemies()
    for k, enemy in pairs(enemies) do
      local enemySpeed = enemy:getSpeed()
      enemy.speedBeforeSlowdown = enemySpeed
      enemy:setSpeed(enemySpeed * speedMultiplier)
    end

    inUse = true
    secondsSinceUse = 0
  end

  function self:update(dt)
    secondsSinceUse = secondsSinceUse + dt

    if inUse and (secondsSinceUse >= SLOWDOWN_RUNTIME) then
      endUse()
    end
  end
end

function pilcrow.creatures.skills.Slowdown.__tostring()
  return "pilcrow.creatures.skills.Slowdown"
end
