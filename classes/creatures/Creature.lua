--[[------------------------------------------------
  ABSTRACT CLASS
]]--------------------------------------------------
pilcrow.creatures.Creature = pilcrow.Object:extend()

function pilcrow.creatures.Creature:new(protected, x, y, sprites, health, speed, points, ox, oy)
  do
    local errorMessage = "Tried to create a pilcrow.creatures.Creature with invalid "
    if tostring(sprites) ~= "Sprites" then
      error(errorMessage .. " Sprites object", 3)
    end
    if health <= 0 or type(health) ~= "number" then
      error(errorMessage .. " health " .. health, 3)
    end
    if speed <= 0 then
      error(errorMessage .. " speed " .. speed, 3)
    end
  end

  local x = x
  local y = y
  local sprites = sprites

  local ox = ox
  local oy = oy

  local points = points or 100
  local direction = "idle"
  local skillTree

  protected.health = health
  local speed = speed

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:calculateDistance(creature)
    return self:calculateDistanceFromPoint(creature:getX(), creature:getY())
  end

  function self:calculateDistanceFromPlayer()
    local player = pilcrow.GameHelper.getInstance():getPlayer()
    local distance = math.sqrt(math.pow(self:getX() - player:getX(), 2) + math.pow(self:getY() - player:getY(), 2))
    return distance
  end

  function self:calculateDistanceFromPoint(x, y)
    local distance = math.sqrt(math.pow(self:getX() - x, 2) + math.pow(self:getY() - y, 2))
    return distance
  end

  function self:canMoveTo(x, y)
    local gameHelper = pilcrow.GameHelper.getInstance()
    if gameHelper:isWallPosition(x, y) or gameHelper:isPropPosition(x, y) or gameHelper:isEnemyPosition(x, y) then
      return false
    end
    return true
  end

  function self:draw()
    love.graphics.draw(
      self:getSprites():getImage(),
      self:getSprites():getQuad(),
      math.floor(self:getX()),
      math.floor(self:getY()),
      0,
      1,
      1,
      self:getOX(),
      self:getOY()
    )
    if pilcrow.debug.Creature.enableHitBoxDraw then
      local spriteW = self:getWidth()
      local spriteH = self:getHeight()
      local x = self:getX() - (spriteW / 2)
      local y = self:getY() - (spriteH / 2)
      love.graphics.rectangle(
        "line", x, y, spriteW, spriteH
      )
      love.graphics.setColor(0, 1, 0)
      love.graphics.rectangle(
        "line", self:getX() - 1, self:getY() - 1, 2, 2
      )
      love.graphics.setColor(1, 1, 1)
    end
  end

  function self:getDirection()
    return direction
  end

  function self:getHealth()
    return protected.health
  end

  function self:getHeight()
    return self:getSprites():getHeight()
  end

  function self:getOX()
    return ox
  end

  function self:getOY()
    return oy
  end

  function self:getPoints()
    return points
  end

  function self:getPosition()
    return x, y
  end

  function self:getSkillTree()
    return skillTree
  end

  function self:getSpeed()
    return speed
  end

  function self:getSprites()
    return sprites
  end

  function self:getWidth()
    return self:getSprites():getWidth()
  end

  function self:getX()
    return x
  end

  function self:getY()
    return y
  end

  function self:newSkillTree(skillTreeName)
    local skillTrees = {}
    skillTrees["Netrunner"] = pilcrow.creatures.skillTrees.Netrunner
    skillTrees["Solo"] = pilcrow.creatures.skillTrees.Solo
    skillTrees["Tech"] = pilcrow.creatures.skillTrees.Tech

    skillTree = skillTrees[skillTreeName](self)
  end

  function self:setDirection(v)
    direction = v
  end

  function self:setOX(v)
    ox = v
  end

  function self:setOY(v)
    oy = v
  end

  function self:setPosition(v, v2)
    x = v
    y = v2
  end

  function self:setSpeed(s)
    speed = s
    sprites:setSpeed(speed)
  end

  function self:setX(v)
    x = v
  end

  function self:setY(v)
    y = v
  end

  function self:subtractHealth(amount)
    protected.health = protected.health - amount
  end

  function self:update(dt)
    self:updateMove(dt)
    if self:getSprites() then
      self:getSprites():setDirection(self:getDirection())
      self:getSprites():update(dt)
    end
    if skillTree then
      skillTree:update(dt)
    end
  end

  function self:updateMove(dt) end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.Creature:__tostring()
  return "pilcrow.creatures.Creature"
end
