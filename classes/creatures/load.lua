pilcrow.creatures = {}

--abstract
require("classes/creatures/Creature")
require("classes/creatures/enemies/Enemy")

--concrete
require("classes/creatures/Player")
require("classes/creatures/enemies/Armor1")
require("classes/creatures/enemies/Armor2")
require("classes/creatures/enemies/Drone")
require("classes/creatures/enemies/FatCyborg")
require("classes/creatures/enemies/Mech")
require("classes/creatures/enemies/YakuzaBoss")
require("classes/creatures/enemies/YakuzaGoon")
