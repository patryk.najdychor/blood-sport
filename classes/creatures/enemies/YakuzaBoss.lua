pilcrow.creatures.YakuzaBoss = pilcrow.creatures.Enemy:extend()

function pilcrow.creatures.YakuzaBoss.constructor(x, y)
  local speed = 200
  local sprites = Sprites("assets/sprites/YakuzaBossSheetWalk", speed, 40, 40)

  local health = 200
  local points = 50
  local targetDistanceFromPlayer = {low = 80, high = 100}
  local ox = 20
  local oy = 20
  local hitSound = love.audio.newSource("/assets/sounds/hit/FatCyborg.wav", "static")
  local deathSound = love.audio.newSource("/assets/sounds/death/FatCyborg.wav", "static")

  return {{x, y, sprites, health, speed, points, ox, oy}, {targetDistanceFromPlayer, hitSound, deathSound}}
end

function pilcrow.creatures.YakuzaBoss:new(protected, x, y)

end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.YakuzaBoss:__tostring()
  return "pilcrow.creatures.YakuzaBoss"
end
