pilcrow.creatures.Mech = pilcrow.creatures.Enemy:extend()

function pilcrow.creatures.Mech.constructor(x, y)
  local speed = 40
  local sprites = Sprites("assets/sprites/Mech1SheetWalk", speed, 64, 64)

  local health = 500
  local points = 200
  local targetDistanceFromPlayer = {low = 100, high = 150}
  local ox = 32
  local oy = 32
  local hitSound = love.audio.newSource("/assets/sounds/hit/FatCyborg.wav", "static")
  local deathSound = love.audio.newSource("/assets/sounds/death/FatCyborg.wav", "static")

  return {{x, y, sprites, health, speed, points, ox, oy}, {targetDistanceFromPlayer, hitSound, deathSound}}
end

function pilcrow.creatures.Mech:new(protected, x, y)

end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.Mech:__tostring()
  return "pilcrow.creatures.Mech"
end
