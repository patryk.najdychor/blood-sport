pilcrow.creatures.skillTrees = {}

require("classes/creatures/skillTrees/SkillTree")
require("classes/creatures/skillTrees/Netrunner")
require("classes/creatures/skillTrees/Solo")
require("classes/creatures/skillTrees/Tech")
